FROM golang:alpine

ADD . /go/src/gitlab.com/sfardiansyah/resource-tkai
WORKDIR /go/src/gitlab.com/sfardiansyah/resource-tkai

ENV PORT=8080

RUN go install ./cmd/...

CMD ["resource-tkai"]