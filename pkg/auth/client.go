package auth

// Client ...
type Client struct {
	ID string `bson:"clientId" json:"clientId"`
}