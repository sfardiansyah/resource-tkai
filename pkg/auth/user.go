package auth

// User ...
type User struct {
  Email string `bson:"email" json:"email"`
  Password string `bson:"password" json:"password"`
  Token string `bson:"token" json:"token"`
}