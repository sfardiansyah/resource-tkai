package auth

import (
	"os"

	"github.com/dgrijalva/jwt-go"
)

// Service ...
type Service interface {
	Login(User) error
	CheckClientID(Client) error
	CreateToken(User) (string, error)
	CheckUserToken(string) error
}

// Repository ...
type Repository interface {
	Login(User) error
	CheckClientID(Client) error
	UpdateUser(User) error
	CheckUserToken(string) error
}

type service struct {
	r Repository
}

// NewService ...
func NewService(r Repository) Service {
	return &service{r}
}

func (s *service) Login(user User) error {
	return s.r.Login(user)
}

func (s *service) CheckClientID(client Client) error {
	return s.r.CheckClientID(client)
}

func (s *service) CheckUserToken(token string) error {
	return s.r.CheckUserToken(token)
}

func (s *service) CreateToken(u User) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	tokenStr, err := token.SignedString([]byte(os.Getenv("SECRET")))
	if err != nil {
		return "", err
	}

	u.Token = tokenStr

	if err = s.r.UpdateUser(u); err != nil {
		return "", err
	}

	return tokenStr, nil
}
