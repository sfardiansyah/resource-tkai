package mongodb

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"time"

	"gitlab.com/sfardiansyah/resource-tkai/pkg/auth"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Storage stores data in MongoDB
type Storage struct {
	db *mongo.Client
}

// NewStorage returns new MongoDB storage
func NewStorage(url string) (*Storage, error) {
	var err error

	if url == "" {
		url = os.Getenv("MONGODB_URL")
	}

	s := new(Storage)

	s.db, err = mongo.NewClient(options.Client().ApplyURI(url))
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	if err = s.db.Connect(ctx); err != nil {
		return nil, err
	}

	return s, nil
}

// ImportUsers ...
func (s *Storage) ImportUsers() error {
	ctx := context.Background()
	collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("users")
	b, err := ioutil.ReadFile("pkg/storage/json/user.json")
	if err != nil {
		return err
	}

	users := make([]interface{}, 0)

	err = json.Unmarshal(b, &users)
	if err != nil {
		return err
	}

	collection.InsertMany(ctx, users)

	return nil
}

// ImportClients ...
func (s *Storage) ImportClients() error {
	ctx := context.Background()
	collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("clients")
	b, err := ioutil.ReadFile("pkg/storage/json/client.json")
	if err != nil {
		return err
	}

	users := make([]interface{}, 0)

	err = json.Unmarshal(b, &users)
	if err != nil {
		return err
	}

	collection.InsertMany(ctx, users)

	return nil
}

// Login ...
func (s *Storage) Login(credentials auth.User) error {
	ctx := context.Background()
	collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("users")
	res := collection.FindOne(ctx, bson.M{"email": credentials.Email})

	if res.Err() != nil {
		return res.Err()
	}

	u := auth.User{}
	if err := res.Decode(&u); err != nil {
    return err
  }

	if u.Password != credentials.Password {
		return errors.New("invalid password")
	}

	return nil
}

// CheckClientID ...
func (s *Storage) CheckClientID(client auth.Client) error {
	ctx := context.Background()
	collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("clients")
	res := collection.FindOne(ctx, bson.M{"clientId": client.ID})

	if res.Err() != nil {
		return res.Err()
	}

	return nil
}

// UpdateUser ...
func (s *Storage) UpdateUser(user auth.User) error {
	ctx := context.Background()
  collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("users")
	res := collection.FindOneAndUpdate(ctx, bson.M{"email": user.Email}, bson.D{{"$set", bson.D{{"token", user.Token}}}})

	if res.Err() != nil {
		return res.Err()
	}

	return nil
}

// CheckUserToken ...
func (s *Storage) CheckUserToken(token string) error {
	ctx := context.Background()
	collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("users")
	res := collection.FindOne(ctx, bson.M{"token": token})

	if res.Err() != nil {
		return res.Err()
	}

	return nil
}
