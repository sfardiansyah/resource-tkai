package rest

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/sfardiansyah/resource-tkai/pkg/auth"
)

// Handler is a function to handle all HTTP Request
func Handler(a auth.Service) http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/", homeHandler())

	s := r.PathPrefix("/api/v1").Subrouter()
	s.HandleFunc("/client", clientHandler(a))
	s.HandleFunc("/login", loginHandler(a))
	s.HandleFunc("/user", userHandler(a))

	return r
}

func homeHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello from Resource!"))
	}
}

func clientHandler(a auth.Service) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
    if r.Method == "GET" {
      if len(r.URL.Query()["clientId"]) == 0 {
        http.Error(w, "clientId not found", http.StatusBadRequest)
        return
      }

      err := a.CheckClientID(auth.Client{ID: r.URL.Query()["clientId"][0]})
      if err != nil {
        http.Error(w, err.Error(), http.StatusUnauthorized)
        return
      }

      w.Write([]byte("{}"))
      return 
    }

		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func userHandler(a auth.Service) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    if r.Method == "GET" {
      if len(r.URL.Query()["token"]) == 0 {
        http.Error(w, "token not found", http.StatusBadRequest)
        return
      }

      err := a.CheckUserToken(r.URL.Query()["token"][0])
      if err != nil {
        http.Error(w, err.Error(), http.StatusUnauthorized)
        return
      }

      w.Write([]byte("{}"))
      return 
    }

		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func loginHandler(a auth.Service) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		u := auth.User{}

    err := json.NewDecoder(r.Body).Decode(&u)
    defer r.Body.Close()

		if err != nil {
      http.Error(w, err.Error(), http.StatusBadRequest)
      return
    } 
    
    if u.Email == "" || u.Password == "" {
      http.Error(w, "request should contains user email and password", http.StatusBadRequest)
      return
    }
    
    t, err := a.CreateToken(u)
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
    
    w.Write([]byte(fmt.Sprintf("{\"token\":\"%s\"}", t)))
	}
}
