package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/sfardiansyah/resource-tkai/pkg/http/rest"
	"gitlab.com/sfardiansyah/resource-tkai/pkg/auth"
	"gitlab.com/sfardiansyah/resource-tkai/pkg/storage/mongodb"
)

func main() {
	s, err := mongodb.NewStorage("")
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	if len(os.Args) == 2 && os.Args[1] == "import" {
		err = s.ImportUsers()
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
    }
    
    err = s.ImportClients()
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}
	} else {
    a := auth.NewService(s)

    r := rest.Handler(a)

    fmt.Printf("Starting beanie at http://localhost:%s/\n", os.Getenv("PORT"))
    log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), r))
  }
}
